from pynput import keyboard



class Input:

    def __init__(self):
        self.__keyPressed = str()

    def appuyerToucheClavier(self):
        direction_key = ["up", "down", "right", "left"]
        menu_key = ["n", "q"]

        def on_press(key):
            try:
                self.__keyPressed = key.name
                return False
            except AttributeError:
                self.__keyPressed = key.char
                return False

        with keyboard.Listener(on_press=on_press) as listener:
            listener.join()
            while self.__keyPressed not in direction_key + menu_key:
                continue
            listener.stop()

        return self.__keyPressed