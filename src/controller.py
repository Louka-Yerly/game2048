import sys as game2048
import random
from gui import Gui
from case import Case
from input import Input


class Controller:

    def __init__(self):
        self.__grille = []
        self.__gui = Gui()
        self.__DIMENSION = 4
        self.__NB_INITIAL = 2
        self.__initialiserPartie()
        self.__input = Input()


    def commencerPartie(self):
        self.__initialiserPartie()
        self.__gui.commencerPartie()
        self.__gui.updateGUI(self.__grille)
        self.__processGame()

    def __initialiserPartie(self):
        self.__supprimerGrille()
        for i in range(0, self.__NB_INITIAL):
            self.__choisirAleatoirementCase()


    def __processGame(self):
        direction_key = {"up": self.__deplacerCasesHaut,
                         "down": self.__deplacerCasesBas,
                         "right": self.__deplacerCasesDroite,
                         "left": self.__deplacerCasesGauche}
        menu_key = {"n": self.commencerPartie,
                    "q": game2048.exit}
        while True:
            key_pressed = self.__input.appuyerToucheClavier()
            if key_pressed in direction_key:
                tab = self.__fusionOk()
                deplacement = self.__deplacementOk(key_pressed)
                if deplacement or (len(tab) != 0):
                    if (len(tab) != 0) and (key_pressed not in tab) and not deplacement:
                        continue
                    direction_key[key_pressed]()
                    self.__choisirAleatoirementCase()
                    print("======================================\n======================================")
                    self.__gui.updateGUI(self.__grille)
                    if self.__case2048Ok():
                        self.__gui.afficherVictoire()
                        while True:
                            key_pressed = self.__input.appuyerToucheClavier()
                            if key_pressed in menu_key:
                                menu_key[key_pressed]()
                                break
                elif not self.__fusionOk() and not self.__detectionCasesLibres():
                    self.__gui.afficherGameOver()
                    while True:
                        key_pressed = self.__input.appuyerToucheClavier()
                        if key_pressed in menu_key:
                            menu_key[key_pressed]()
                            break
            else:
                menu_key[key_pressed]()


    def __supprimerGrille(self):
        self.__grille = self.__grille = [[Case(0) for i in range(0, self.__DIMENSION)] for j in range(0, self.__DIMENSION)]

    def __deplacerCasesGauche(self):
        # Move the case
        def move():
            for row, listCase in enumerate(self.__grille):
                newListCase = []
                for col, specificCase in enumerate(listCase):
                    if specificCase.getValue() != 0:
                        newListCase.append(specificCase)

                while len(newListCase) < self.__DIMENSION:
                    newListCase.append(Case(0))
                self.__grille[row] = newListCase

        # Fusionne
        def fusionne():
            for row, listCase in enumerate(self.__grille):
                col = 0
                while col < self.__DIMENSION-1:
                    if listCase[col].getValue() == listCase[col+1].getValue() and listCase[col].getValue() != 0:
                        listCase[col].setValue(2*listCase[col].getValue())
                        listCase[col+1].setValue(0)
                    col += 1
        # Call the function
        move()
        fusionne()
        move()

    def __deplacerCasesDroite(self):
        def move():
            for row, listCase in enumerate(self.__grille):
                newListCase = []
                for col in reversed(range(len(listCase))):
                    if listCase[col].getValue() != 0:
                        newListCase.append(listCase[col])

                while len(newListCase) < self.__DIMENSION:
                    newListCase = [Case(0)] + newListCase
                self.__grille[row] = newListCase

        def fusionne():
            for row, listCase in enumerate(self.__grille):
                col = self.__DIMENSION-1
                while col >= 1:
                    if listCase[col].getValue() == listCase[col - 1].getValue() and listCase[col].getValue() != 0:
                        listCase[col].setValue(2 * listCase[col].getValue())
                        listCase[col - 1].setValue(0)
                    col -= 1

        # Call the function
        move()
        fusionne()
        move()

    def __deplacerCasesHaut(self):
        def move():
            for col in range(self.__DIMENSION):
                newListCase = []
                for row in range(self.__DIMENSION):
                    specificCase = self.__grille[row][col]
                    if specificCase.getValue() != 0:
                        newListCase.append(specificCase)
                        self.__grille[row][col] = Case(0)

                for row, specificCase in enumerate(newListCase):
                    self.__grille[row][col] = specificCase

        def fusionne():
            for col in range(self.__DIMENSION):
                row = 0
                while row < self.__DIMENSION - 1:
                    if self.__grille[row][col].getValue() == self.__grille[row+1][col].getValue()\
                            and self.__grille[row][col].getValue() != 0:
                        self.__grille[row][col].setValue(2 * self.__grille[row][col].getValue())
                        self.__grille[row+1][col].setValue(0)
                    row += 1

        # Call the function
        move()
        fusionne()
        move()

    def __deplacerCasesBas(self):
        def move():
            for col in range(self.__DIMENSION):
                newListCase = []
                for row in reversed(range(self.__DIMENSION)):
                    specificCase = self.__grille[row][col]
                    if specificCase.getValue() != 0:
                        newListCase.append(specificCase)
                        self.__grille[row][col] = Case(0)

                for row, specificCase in enumerate(newListCase):
                    self.__grille[self.__DIMENSION-row-1][col] = specificCase

        def fusionne():
            for col in range(self.__DIMENSION):
                row = self.__DIMENSION - 1
                while row >= 1:
                    if self.__grille[row][col].getValue() == self.__grille[row-1][col].getValue()\
                            and self.__grille[row][col].getValue() != 0:
                        self.__grille[row][col].setValue(2 * self.__grille[row][col].getValue())
                        self.__grille[row-1][col].setValue(0)
                    row -= 1

        # Call the function
        move()
        fusionne()
        move()

    def __deplacementOk(self, direction):
        def deplacement():
            for row, listCase in enumerate(self.__grille):
                for col, specificCase in enumerate(listCase):
                    if specificCase.getValue() == 0:
                        if direction == "left":
                            for x in range(col, self.__DIMENSION):
                                if self.__grille[row][x].getValue() != 0:
                                    return True
                        if direction == "right":
                            x = col
                            for x in range(col):
                                if self.__grille[row][x].getValue() != 0:
                                    return True
                        if direction == "up":
                            x = row
                            for x in range(row, self.__DIMENSION):
                                if self.__grille[x][col].getValue() != 0:
                                    return True
                        if direction == "down":
                            x = row
                            for x in range(row):
                                if self.__grille[x][col].getValue() != 0:
                                    return True

        return deplacement()

    def __fusionOk(self):
        tab = []
        for row in range(self.__DIMENSION-1):
            # if len(tab) != 0:
                # break
            for col in range(self.__DIMENSION-1):
                if self.__grille[row][col] == self.__grille[row][col+1] and self.__grille[row][col] != 0:
                    tab += ["left", "right"]
                     # break
                if self.__grille[row][col] == self.__grille[row+1][col] and self.__grille[row][col] != 0:
                    tab += ["up", "down"]
                    break

        for col in range(self.__DIMENSION-1):
            if self.__grille[self.__DIMENSION-1][col] == self.__grille[self.__DIMENSION-1][col + 1] and self.__grille[self.__DIMENSION-1][col] != 0:
                tab += ["left", "right"]
                break

        for row in range(self.__DIMENSION-1):
            if self.__grille[row][self.__DIMENSION-1] == self.__grille[row+1][self.__DIMENSION-1] and self.__grille[row][self.__DIMENSION-1] != 0:
                tab += ["up", "down"]
                break

        return tab

    def __detectionCasesLibres(self):
        for row in self.__grille:
            for speceficCase in row:
                if speceficCase.getValue() == 0:
                    return True
        return False

    def __choisirAleatoirementCase(self):
        while True:
            row = random.randint(0, self.__DIMENSION - 1)
            col = random.randint(0, self.__DIMENSION - 1)
            if self.__grille[row][col].getValue() == 0:
                break
        value = 2 if random.randint(0, 1) == 0 else 4
        c = Case(value)
        self.__grille[row][col] = c

    def __case2048Ok(self):
        for row in self.__grille:
            for speceficCase in row:
                if speceficCase.getValue() == 2048:
                    return True
        return False