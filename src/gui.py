import controller
import os

class Gui:
    def __init__(self):
        self.__GAME = str()
        self.__output = list()
        self.__COMMAND = "\'q\' to quit or \'n\' for new game. Use the arrows to move the numbers"

    def commencerPartie(self):
        self.__GAME = [
            "+-------------------------------------+",
            "| +------+ +------+ +------+ +------+ |",
            "| |      | |      | |      | |      | |",
            "| +------+ +------+ +------+ +------+ |",
            "| +------+ +------+ +------+ +------+ |",
            "| |      | |      | |      | |      | |",
            "| +------+ +------+ +------+ +------+ |",
            "| +------+ +------+ +------+ +------+ |",
            "| |      | |      | |      | |      | |",
            "| +------+ +------+ +------+ +------+ |",
            "| +------+ +------+ +------+ +------+ |",
            "| |      | |      | |      | |      | |",
            "| +------+ +------+ +------+ +------+ |",
            "+-------------------------------------+"]

        self.__output = [
                "+-------------------------------------+",
                "| +------+ +------+ +------+ +------+ |",
                "| |      | |      | |      | |      | |",
                "| +------+ +------+ +------+ +------+ |",
                "| +------+ +------+ +------+ +------+ |",
                "| |      | |      | |      | |      | |",
                "| +------+ +------+ +------+ +------+ |",
                "| +------+ +------+ +------+ +------+ |",
                "| |      | |      | |      | |      | |",
                "| +------+ +------+ +------+ +------+ |",
                "| +------+ +------+ +------+ +------+ |",
                "| |      | |      | |      | |      | |",
                "| +------+ +------+ +------+ +------+ |",
                "+-------------------------------------+"]

    def updateGUI(self, grille):
        self.__output = self.__GAME
        mappageRow = {0: 1, 1: 4, 2: 7, 3: 10}
        mappageCol = {0: 2, 1: 11, 2: 20, 3: 29}
        lengthRow = 3
        lengthCol = 8

        for row, listCase in enumerate(grille):
            for col, specificCase in enumerate(listCase):
                if grille[row][col] == 0:
                    for i in range(lengthRow):
                        # Delete the case
                        sString = self.__output[mappageRow[row]+i]
                        sString = sString[0:mappageCol[col]] + "        " + sString[mappageCol[col] + lengthCol:]
                        self.__output[mappageRow[row]+i] = sString

                else:
                    # set the number
                    sString = self.__output[mappageRow[row]]
                    self.__output[mappageRow[row]] = sString[0:mappageCol[col]] + "+------+" + sString[mappageCol[col] + lengthCol:]

                    sString = self.__output[mappageRow[row]+2]
                    self.__output[mappageRow[row]+2] = sString[0:mappageCol[col]] + "+------+" + sString[mappageCol[
                                                                                                           col] + lengthCol:]
                    number = str(grille[row][col].getValue())
                    space = ""
                    for i in range(lengthCol-len(number)-3):
                        space += " "
                    sString = self.__output[mappageRow[row]+1]
                    sString = sString[0:mappageCol[col]] + "| " + number + space + "|" + sString[mappageCol[col] + lengthCol:]
                    self.__output[mappageRow[row]+1] = sString

        for line in self.__output:
            print(line)
        print(self.__COMMAND)

    def afficherGameOver(self):
        gameover = """
         ___                 ___              
        / __|__ _ _ __  ___ / _ \\__ _____ _ _ 
       | (_ / _` | '  \\/ -_) (_) \\ V / -_) '_|
        \\___\\__,_|_|_|_\\___|\\___/ \\_/\\___|_|                              
        """
        print(gameover)
        print(self.__COMMAND)

    def afficherVictoire(self):
        victoire = """
        __ _(_)__| |_ ___(_)_ _ ___ 
        \\ V / / _|  _/ _ \\ | '_/ -_)
         \\_/|_\\__|\\__\\___/_|_| \\___|"""
        print(victoire)
        print(self.__COMMAND)
