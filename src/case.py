class Case:
    def __init__(self, value):
        self.__value = value

    def getValue(self):
        return self.__value

    def setValue(self, value):
        self.__value = value

    def __str__(self):
        return str(self.getValue())

    def __repr__(self):
        return str(self.getValue())

    def __eq__(self, other):
        if type(other) == int:
            return self.getValue() == other
        elif type(other) == Case:
            return self.getValue() == other.getValue()
        else:
            raise TypeError("")
